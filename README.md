# 開発開始時:
1. コンテナ起動:
```bash
host$ docker-compose up -d
```
2. コンテナ内部にアタッチ:
```bash
host$ docker-compose exec node bash
```
3. npmコマンド実行:
```bash
container$ npm run dev
# Typescriptのファイル監視とnodeが起動する
```

# 開発データストアのリセット方法:

## A: コンテナ自体を削除
```bash
host$ docker-compose down
```
